#!/usr/bin/env sh

# POSIX-compliant shell script to retrieve university restaurant menu
# for the day.
# Has a few convenience functions for Nix users. Caches the web page
# so that frequent menu-checking doesn't cause you to be blocked by
# the overly-aggressive firewall.

# Author: Louis Boulanger <louis.boulanger@univ-grenoble-alpes.fr>

#--- Program arguments ---#
ARGS="$@"
TODAY=0
FORCE_UNCACHED=0
VERBOSE=0

usage() {
    2>&1 echo "usage: $0 [-t]"
    2>&1 echo "arguments:"
    2>&1 echo "  -h, --help: display this help message and exit."
    2>&1 echo "  -t, --today: only display menu for today's date."
    2>&1 echo "  -f, --force-uncached: forces fetching the menu from the internet instead of using the cached version."
    2>&1 echo "  -v, --verbose: enable debug prints."
}

while :; do
    case "$1" in
	-h|-\?|--help)
	    usage
	    exit
	    ;;

	-t|--today)
	    TODAY=1
	    ;;
	-f|--force-uncached)
	    FORCE_UNCACHED=1
	    ;;
	-v|--verbose)
	    VERBOSE=1
	    ;;
	-?*)
	    usage
	    exit 1
	    ;;
	*)
	    break
    esac
    shift
done

#--- Helper functions ---#
# Prints something only if the --verbose flag was set. Basically a
# wrapper around `echo`.
#
# Arguments:
#   The arguments to pass to `echo`.
#
# Returns:
#   The return value of `echo` if executed, 0 otherwise.
verbose_print() {
    if [ "$VERBOSE" -eq "1" ]; then
	2>&1 echo "$@"
    fi
}

# Check if program is available in path
#
# Arguments:
#   1. The command to test
#
# Returns:
#   The output of the `command` command.
is_available() {
    command -v "$1" &> /dev/null
}

# Check program dependencies and exits if one is missing.
#
# Arguments:
#  1. The list of dependencies to check
#
# Returns:
#  Does not return if a dependency is missing; otherwise returns 0.
check_dependencies() {
    verbose_print "[?] Checking if dependencies are present on PATH:"
    for DEP in $1; do
	if ! is_available "$DEP"; then
	    verbose_print "[!]   - $DEP: NOT AVAILABLE"
	    2>&1 echo "dependency not met: $DEP: command not found."
	    exit 1
	else
	    verbose_print "[.]   - $DEP: AVAILABLE"
	fi
    done
}

# Ensures that the environment for this script is set.
# In environments where the `nix` package manager exists, it starts a
# `nix-shell` with the dependencies needed. Otherwise, this function
# calls `check_dependencies`.
#
# Arguments:
#   1. The list of dependencies of the program
#   2. The list of nix packages to install.
#
# Returns:
#   - If the script enters a nix-shell, it returns the return value of
#     the program executed with the correct dependencies.
#
#   - If the script is already in a nix-shell, and the dependencies
#     are still not met, this function does not return (see
#     `check_dependencies`). Otherwise, it returns 0.
#
#   - If `nix` is not available in the environment, this function
#     returns according to `check_dependencies`.
ensure_env() {
    verbose_print "[?] Beginning to ensure script environment"
    if is_available nix-shell; then
	verbose_print "[?] Nix is available, checking if currently in subshell..."
	if [ -v IN_NIX_SHELL ]; then
	    verbose_print "[.]   In Nix subshell, ensuring commands are available."
	    check_dependencies "$1"
	else
	    verbose_print "[.]   Not in Nix subshell, starting one with the following dependencies: [$2]."
	    verbose_print "--- Nix shell spawned ---"
	    nix-shell -p "[$2]" --command "$0 $ARGS"
	    EXIT=$?
	    verbose_print "--- Nix shell ended ---"
	    verbose_print "[.] Nix subshell ended, exiting."
	    exit $EXIT
	fi
    else
	verbose_print "[.] Nix unavailable, checking if dependencies are available natively."
	check_dependencies "$1"
    fi
}

# Parses an HTML document according to a rule and returns the results.
#
# Arguments:
#   1. The HTML document as a string
#   2. The `xmllint` xpath rule
#
# Returns:
#   Non-zero value if an error occured.
#
# Outputs:
#   The list of matches in the HTML document.
parse_html() {
    echo "$1" | xmllint --html --encode UTF-8 --xpath "$2" 2>/dev/null -
}

# Parses partial HTML, by wrapping the content into a bare-minimum HTML prelude.
#
# Arguments:
#   1. The partial HTML content to parse, as a string
#   2. The `xmllint` xpath rule
#
# Returns:
#   Non-zero value if an error occured.
#
# Outputs:
#   The list of matches in the partial HTML content.
parse_partial_html() {
    parse_html "<html><head><meta charset=\"utf-8\"></head><body>$1</body></html>" "$2"
}

#--- Variables ---#

# Today's date
DATE=`date -I`

# Temporary file name for caching
TMPFILE="/tmp/crousmiams.$DATE"

# Address of menu web page
ADDR="https://www.crous-grenoble.fr/restaurant/ru-diderot/"

# Program dependencies
DEPS="curl xmllint"

# Nix packages to install for dependencies
DEPS_NIX_PKGS="curl libxml2"

#--- Program ---#
# Ensure the environment; after this line, we have all the programs we
# need.
ensure_env "$DEPS" "$DEPS_NIX_PKGS"

# If the --force-uncached flag is set, fetch the webpage regardless of
# the presence or abscence of a cached version.
if [ "$FORCE_UNCACHED" -eq "1" ]; then
    verbose_print "[.] --force-uncached flag passed, downloading web page."
    HTML=`curl -s "$ADDR"`
else
    # Fetch the web page, but only if it is not cached.
    if ! [ -f "$TMPFILE" ]; then
	verbose_print "[.] Temporary file $TMPFILE not present, downloading and caching."
	curl -s "$ADDR" > "$TMPFILE"
    fi
    verbose_print "[.] Using cached version at $TMPFILE"
    HTML=`cat $TMPFILE`
fi

# Parses the web page to retrieve only the dates of the menus.
DATES=`parse_html "$HTML" "//div[@class='menu_date']/time/text()"`
COUNTER=1

while IFS= read -r DATE; do
    # Output the date
    echo "$DATE"
    
    # Parses the HTML for the menu of that specific date, using the COUNTER variable.
    MENU=`parse_html "$HTML" "(//ul[@class='meal_foodies'])[$COUNTER]/li"`

    # Print each item of the menu.
    while IFS= read -r ITEM; do
	TITLE=`parse_partial_html "$ITEM" "(//li)[1]/text()"`
	echo "  $TITLE"
	parse_partial_html "$ITEM" "//ul/li/text()" | while read ITEM; do
	    echo "    $ITEM"
	done
    done <<EOF
    $MENU
EOF

    # Exit early if the "today" flag was passed to the executable.
    if [ "$TODAY" -eq "1" ]; then
	break
    fi
    COUNTER=$((COUNTER + 1))
    echo ""
done <<EOF
$DATES
EOF
