# Crousmiams: check the RU menu from the CLI
Born from procrastination, crafted with love, here is the most over-engineered, useless yet useful, shell script.

## Features
- Downloads, parses and displays the menu of the Direrot RU at the Grenoble Campus
- Caches the webpage so that you don't get banned from looking at the webpage
- Verbose output for extra rizz
- Wanna download the web page anyways? Sure, here's a command flag for that, I guess
- If you only care for today's menu and not the next two week's, you can just do that. I should probably add something to see the N next days, but "flemme". If you want to do it yourself, look at the `COUNTER` variable and do something cool with it.

## Requirements
- A working `/usr/bin/env` program that knows about a `sh` program somewhere.
- If you have Nix installed:
  - A working `nix-shell` command in your `$PATH`
- Without Nix:
  - `curl`
  - `libxml2`
  
## Why do you invoke `nix-shell` directly in your script instead of making a `shell.nix` file or a flake
Why not! It's fun, it works and it keeps everything in a neat little file.

## Why this script when you can visit the webpage?
Maybe you don't have a graphical interface. Maybe you can't find the
webpage because it is buried in the maze of Crous' website. The latter
is more likely.

## Did you seriously make this instead of meaningful work?
Prefer not to answer.
